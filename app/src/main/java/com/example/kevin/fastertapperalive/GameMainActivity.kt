package com.example.kevin.fastertapperalive

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.util.Log
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class GameMainActivity : AppCompatActivity() {

    internal lateinit var gameScoreTextView : TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal var score = 0
    internal var counDownInterval = 1000L
    internal var gameStarted = false
    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown = 10000L
    internal var timeLeft = 10

    internal val TAG = GameMainActivity::class.java.simpleName

    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("tap-score")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)

        Log.d(TAG, "onCreate called. Score is $score")


        gameScoreTextView = findViewById(R.id.game_scoreView)
        timeLeftTextView = findViewById(R.id.game_timeView)
        tapMeButton = findViewById(R.id.game_tap_button)

        tapMeButton.setOnClickListener{
            _ -> incrementScore()
        }

        if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }
        else{
            resetGame()
        }

    }

    companion object {
        private val SCORE_KEY = "SCORE_PLAY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    private fun resetGame(){
        score = 0
        timeLeft = 10


        val gameScore = getString(R.string.game_score , Integer.toString(score))
        gameScoreTextView.text =   gameScore

        val timeLeftText = getString(R.string.game_time, Integer.toString(timeLeft))
        timeLeftTextView.text = timeLeftText

        countDownTimer = object : CountDownTimer(initialCountDown,counDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                timeLeftTextView. text = getString(R.string.game_time,Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }
    }


    private fun startGame (){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over_message,Integer.toString(score)),Toast.LENGTH_LONG).show()
        showSaveScoreDialog(Integer.toString(score))
        resetGame()
    }

    private fun incrementScore(){
        score ++

        //val newScore = "Score: " + Integer.toString(score);
        val newScore = getString( R.string.game_score, Integer.toString(score))



        gameScoreTextView.text = newScore

        if(!gameStarted){
            startGame()
        }

    }

    private fun showSaveScoreDialog(scoreToSave : String){
        val dialogTitle = "Inserte su Nick"
        val positiveButtonTitle = "Guardar"

        val builder = AlertDialog.Builder(this)
        val listTitleEditText = EditText(this)

        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle){
            dialog, i ->
            val nick = listTitleEditText.text.toString()
            val newId = UUID.randomUUID().toString()

            ref.child(newId).
                    child("score").
                    setValue(scoreToSave)

            ref.child(newId).
                    child("id").
                    setValue(nick)
            dialog.dismiss()
        }
        builder.create().show()
    }



    private fun restoreGame(){
        val restoreScore = getString(R.string.game_score,Integer.toString(score))
        gameScoreTextView.text =  restoreScore

        val restoredTime = getString(R.string.game_time, Integer.toString(timeLeft))
        timeLeftTextView.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, counDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

                timeLeftTextView.text = Integer.toString(timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
        countDownTimer.start()
        gameStarted = true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putInt(SCORE_KEY,score)
        outState.putInt(TIME_LEFT_KEY,timeLeft)
        countDownTimer.cancel()

        Log.d(TAG, "onSaveInstancesState: score = $score & timeleft = $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")
    }

}
